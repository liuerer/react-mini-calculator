import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

  constructor(props){
    super(props);

    this.state={
      result: 0
    }
  }
  handleClickAddOne(){
    this.setState({
      result: this.state.result + 1
    });
  }
  handleClickReduceOne(){
    this.setState({
      result: this.state.result - 1
    });
  }
  handleClickMultiplyTwo(){
    this.setState({
      result: this.state.result * 2
    });
  }
  handleClickDivideTwo(){
    this.setState({
      result: this.state.result / 2
    });
  }

  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.result} </span>
        </div>
        <div className="operations">
          <button onClick={() => this.handleClickAddOne()}>加1</button>
          <button onClick={() => this.handleClickReduceOne()}>减1</button>
          <button onClick={() => this.handleClickMultiplyTwo()}>乘以2</button>
          <button onClick={() => this.handleClickDivideTwo()}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

